//
// This file (and all other Swift source files in the Sources directory of this playground) will be precompiled into a framework which is automatically made available to Protocols.playground.
//

import Foundation

public protocol FirstProtocol {
    // protocol definition goes here
}

public protocol AnotherProtocol {
    // protocol definition goes here
}

public class ​SomeSuperClass​: NSObject {
    
}

protocol SomeInheritedProtocol {
    
}