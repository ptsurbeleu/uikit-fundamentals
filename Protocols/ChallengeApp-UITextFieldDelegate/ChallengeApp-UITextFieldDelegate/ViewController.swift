//
//  ViewController.swift
//  ChallengeApp-UITextFieldDelegate
//
//  Created by Pavel Tsurbeleu on 5/31/15.
//  Copyright (c) 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var cashAmountTextField: UITextField!
    @IBOutlet weak var switchClauseTextField: UITextField!
    @IBOutlet weak var textClauseSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

