//
//  LockableTextFieldDelegate.swift
//  ChallengeApp
//
//  Created by Pavel Tsurbeleu on 6/25/15.
//  Copyright © 2015 Pavel Tsurbeleu. All rights reserved.
//

import Foundation
import UIKit

class LockableTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    typealias LockState = () -> Bool
    
    let lockerState: LockState
    
    init(lockerState: LockState) {
        self.lockerState = lockerState
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        return lockerState()
    }
}