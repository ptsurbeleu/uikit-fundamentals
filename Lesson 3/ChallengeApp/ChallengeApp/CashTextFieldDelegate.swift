//
//  CashTextFieldDelegate.swift
//  ChallengeApp
//
//  Created by Pavel Tsurbeleu on 6/24/15.
//  Copyright © 2015 Pavel Tsurbeleu. All rights reserved.
//

import Foundation
import UIKit

class CashTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    let precision: Double = 0.01

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let formatter = NSNumberFormatter()
        // Show as many as 11 digits after decimals
        formatter.maximumIntegerDigits = 11
        // Show as minimum as 1 digit after decimals
        formatter.minimumIntegerDigits = 1
        // Show as minimum & maximum as 2 decimal places
        formatter.maximumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        // Format numbers as currency of the current locale
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        
        let cashString = build(textField.text!, range: range, withString: string)
                            .stringByReplacingOccurrencesOfString(formatter.currencySymbol, withString: "")
                            .stringByReplacingOccurrencesOfString(formatter.currencyDecimalSeparator, withString: "")
                            .stringByReplacingOccurrencesOfString(formatter.currencyGroupingSeparator, withString: "")
        
        let cashAmount = (cashString as NSString).doubleValue * precision
        textField.text = formatter.stringFromNumber(cashAmount)
        
        return false
    }
    
    func build(text: String, range: NSRange, withString: String) -> String {
        
        let start = advance(text.startIndex, range.location)
        let end = advance(start, range.length)
        let stringRange = Range<String.Index>(start: start, end: end)
        let finalString = text.stringByReplacingCharactersInRange(stringRange, withString: withString)
        
        return finalString
    }
}