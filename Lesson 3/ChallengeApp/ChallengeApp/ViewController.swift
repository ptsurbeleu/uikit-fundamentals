//
//  ViewController.swift
//  ChallengeApp
//
//  Created by Pavel Tsurbeleu on 6/23/15.
//  Copyright © 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

extension UIControl {
    
    func dismissKeyboard() {
        // hide keyboard if the control is currently the first responder
        if isFirstResponder() {
            resignFirstResponder()
        }
    }
}

class ViewController: UIViewController {
    
    @IBOutlet weak var fieldOfZipCode: UITextField!
    @IBOutlet weak var fieldOfCashAmount: UITextField!
    @IBOutlet weak var fieldOfLockableState: UITextField!
    @IBOutlet weak var switchOfLockableState: UISwitch!

    let zipcodeDelegate = ZipCodeTextFieldDelegate()
    let cashDelegate = CashTextFieldDelegate()
    var lockDelegate: LockableTextFieldDelegate!
    
    @IBAction func onSwitchTapped(sender: UISwitch, forEvent event: UIEvent) {
        
        fieldOfLockableState.dismissKeyboard()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        lockDelegate = LockableTextFieldDelegate(
            lockerState: { return self.switchOfLockableState.on })
     
        fieldOfZipCode.delegate = zipcodeDelegate
        fieldOfCashAmount.delegate = cashDelegate
        fieldOfLockableState.delegate = lockDelegate
    }

}

