//
//  ZipCodeTextFieldDelegate.swift
//  ChallengeApp
//
//  Created by Pavel Tsurbeleu on 6/23/15.
//  Copyright © 2015 Pavel Tsurbeleu. All rights reserved.
//

import Foundation
import UIKit

class ZipCodeTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let tag = FieldTag(rawValue: textField.tag)
        if tag != FieldTag.zipCode {
            return true
        }
        
        let count = countOfString(textField.text) + countOfString(string)
        
        return count <= 5 || range.length > 0
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        print("sample text when finished editing => \(textField.text)")
    }
    
    func countOfString(text: String?) -> Int {
        
        return text?.characters.count ?? 0
    }
}

enum FieldTag: Int {
    
    case notSet, zipCode, cash, lockable
}