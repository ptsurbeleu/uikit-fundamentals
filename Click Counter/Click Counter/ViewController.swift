//
//  ViewController.swift
//  Click Counter
//
//  Created by Pavel Tsurbeleu on 3/5/15.
//  Copyright (c) 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var count = 0
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelMirror: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalColor = view.backgroundColor
    }
    
    @IBAction func incrementCount() {
        count++
        updateLabel()
    }

    @IBAction func decrementCount(sender: UIButton, forEvent event: UIEvent) {
        count--
        updateLabel()
    }
    
    var originalColor: UIColor!
    
    @IBAction func toggleBackground(sender: UIButton) {
        if view.backgroundColor == originalColor {
            view.backgroundColor = UIColor.purpleColor()
        } else {
            view.backgroundColor = originalColor
        }
    }
    
    func updateLabel() {
        let text = "\(count)"
        label.text = text
        labelMirror.text = text
    }
}

