//
//  ViewController.swift
//  StockViewControllers
//
//  Created by Pavel Tsurbeleu on 6/20/15.
//  Copyright © 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func experiment(sender: UIButton, forEvent event: UIEvent) {
        
        experimentWithAlertView()
    }
    
    func experimentWithAlertView() {
        
        let controller = UIAlertController(title: "title", message: "message", preferredStyle: UIAlertControllerStyle.Alert)
        
        controller.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    func experimentWithActivityView() {
        
        let image = UIImage()
        let controller = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    func experimentWithImagePicker() {
        
        let nextController = UIImagePickerController()
        self.presentViewController(nextController, animated: true, completion: nil)
    }

}

