//
//  ViewController.swift
//  Roshambo
//
//  Created by Pavel Tsurbeleu on 3/20/15.
//  Copyright (c) 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let rock = 1
    let paper = 2
    let scissors = 3
    
    @IBOutlet var buttonOne: UIButton!
    @IBOutlet var buttonTwo: UIButton!
    @IBOutlet var buttonThree: UIButton!

    @IBAction func presentWithCode() {
        var controller: ResultsViewController
        
        controller = instantiateFromStoryboard(self, "showResults")
        
        self.initResultsView(controller, playerOneMove: rock)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    @IBAction func triggerSegueWithCode() {
        self.performSegueWithIdentifier("manualSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "manualSegue" {
            self.initResultsView(segue.target(), playerOneMove: paper)
        }
        
        if segue.identifier == "buttonSegue" {
            self.initResultsView(segue.target(), playerOneMove: scissors)
        }
    }
    
    func secondPlayerMove() -> Int {
        
        let value = 1 + arc4random() % 3
        return Int(value)
    }
    
    func resolveGameState(yourMove: Int, theirMove: Int) -> GameState {
        
        if yourMove != theirMove {
            let gameState = yourMove + theirMove
            
            if gameState == (rock + paper) {
                let winner = yourMove == paper ? "You" : "They"
                return ("PaperCoversRock", "Paper covers Rock. \(winner) Win!")
            }
            
            if gameState == (rock + scissors) {
                let winner = yourMove == rock ? "You" : "They"
                return ("RockCrushesScissors", "Rock crushes Scissors. \(winner) Win!")
            }
            
            if gameState == (paper + scissors) {
                let winner = yourMove == scissors ? "You" : "They"
                return ("ScissorsCutPaper", "Scissors cut Paper. \(winner) Win!")
            }
        }
        
        // Its a tied game in most cases...
        // However, there is a chance that numbers might have changed and you coud get here because of a bug
        return ("itsATie", "It's a Tie...")
    }
    
    func initResultsView(controller: ResultsViewController, playerOneMove: Int) {
        let playerTwoMove = self.secondPlayerMove()
        let gameState = self.resolveGameState(playerOneMove, theirMove: playerTwoMove)
        controller.challengeOutcome = gameState
    }
}

extension UIStoryboardSegue {
    func target<T>() -> T {
        return self.destinationViewController as T
    }
}

func instantiateFromStoryboard<T>(controller: UIViewController, identifier: String) -> T {
    var target: T
    target = controller.storyboard?.instantiateViewControllerWithIdentifier(identifier) as T
    return target;
}