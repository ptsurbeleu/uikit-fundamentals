//
//  ResultsViewController.swift
//  Roshambo
//
//  Created by Pavel Tsurbeleu on 3/21/15.
//  Copyright (c) 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    var challengeOutcome: GameState!
    
    @IBOutlet var outcome: UIImageView!
    @IBOutlet var message: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        
        self.message.text = self.challengeOutcome.message
        self.outcome.image = UIImage(named: self.challengeOutcome.state)
        self.outcome.alpha = 0
    }
    
    override func viewDidAppear(animated: Bool) {
        
        UIView.animateWithDuration(0.3) {
            self.outcome.alpha = 1
        }
    }
    
    @IBAction func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
